package pl.edu.pwsztar.domain.dto;

public class MovieTrailerDto {
    private String movieId;
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getVideoId() {
        return movieId;
    }

    public void setVideoId(String movieId) {
        this.movieId = movieId;
    }
}
